#include <iostream>
#include <map>
#include <SFML/Graphics.hpp>

#include <AndyFind/Game/Game.h>
#include <AndyFind/Display/UI.h>
#include <AndyFind/Models/Map/MapFactory/SimpleMapFactory.h>

int main() {

    std::cout << "Starting ..." << std::endl;
    Display::UI ui(1000, 800);

    ui.start();
}