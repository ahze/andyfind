#include <map>
#include <iostream>

#include <AndyFind/Controller/Controller.h>

using namespace Misc;

Controller::Controller::Controller() {
    putKey(Misc::Direction::Up, sf::Keyboard::Z);
    putKey(Misc::Direction::Down, sf::Keyboard::S);
    putKey(Misc::Direction::Left, sf::Keyboard::Q);
    putKey(Misc::Direction::Right, sf::Keyboard::D);

    putKey(Model::Element::Red, sf::Keyboard::K);
    putKey(Model::Element::Green, sf::Keyboard::L);
    putKey(Model::Element::Blue, sf::Keyboard::M);
}

void Controller::Controller::putKey(const Direction d, const sf::Keyboard::Key &k) {
    m_dirMap[d] = k;
}

void Controller::Controller::putKey(const Model::Element::Element e, const sf::Keyboard::Key &k) {
    m_elemMap[e]  = k;
}

bool Controller::Controller::isDirectionPressed(const Direction dir) {
    return sf::Keyboard::isKeyPressed(m_dirMap[dir]);
}

bool Controller::Controller::isElementPressed(const Model::Element::Element element) {
    return sf::Keyboard::isKeyPressed(m_elemMap[element]);
}
