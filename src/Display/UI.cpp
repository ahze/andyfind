
#include <chrono>
#include <thread>
#include <SFML/Graphics.hpp>
#include <AndyFind/Display/UI.h>
#include <AndyFind/Display/FpsDisplay.h>
#include <AndyFind/Display/EntityDebug.h>


#define TIME 1000000.0f
#define UNIT microseconds

Display::UI::UI(int width, int height) : m_window(sf::VideoMode(width, height, 30),"AndyFind"),
                                         m_view(sf::FloatRect(0, 0, width, height)),
                                         m_upLeftCorner(0, 0, false),
                                         m_game(),
                                         m_cameraTarget(m_game.getPlayer()){

    m_additionalComponents.push_back(std::make_unique<FpsDisplay>());
    m_additionalComponents.push_back(std::make_unique<EntityDebug>());
}

void Display::UI::updateView() {
    m_window.setView(m_view);
}
void Display::UI::setViewOnTarget(){
    Vector2 windowSize = Vector2(m_view.getSize(), false);
    Vector2 cameraTarget = m_cameraTarget.getPos().toPixel() + m_cameraTarget.getDrawDiff();
    m_upLeftCorner = cameraTarget - windowSize / 2;
    Vector2 mapSize = m_game.getMap().getSize().toPixel();

    if(m_upLeftCorner.m_x < 0) m_upLeftCorner.m_x = 0;
    if(m_upLeftCorner.m_x + windowSize.m_x > mapSize.m_x) m_upLeftCorner.m_x = mapSize.m_x - windowSize.m_x;
    if(m_upLeftCorner.m_y < 0) m_upLeftCorner.m_y = 0;
    if(m_upLeftCorner.m_y + windowSize.m_y > mapSize.m_y) m_upLeftCorner.m_y = mapSize.m_y - windowSize.m_y;

    m_view.setCenter((m_upLeftCorner + windowSize / 2).toPixel().vector2F());
    updateView();
}


void Display::UI::display(Gamens::Game &game) {
    m_window.clear(sf::Color::White);
    setViewOnTarget();
    game.draw(m_window, sf::RenderStates());

    for(auto& component : m_additionalComponents){
        component->update(*this);
        m_window.draw(*component);
    }




    m_window.display();
}

void Display::UI::resetView() {
    Vector2 v(getWindow().getSize(), false);
    v = v/2;
    getView().setCenter(v.vector2F());
    updateView();
}

void Display::UI::start() {

    auto neededDelay = (long)(TIME / Display::fps);

    while(m_game.isRunning()){
        long start = std::chrono::duration_cast<std::chrono::UNIT>(std::chrono::system_clock::now().time_since_epoch()).count();

        manageEvents();
        m_game.update(m_game);
        display(m_game);

        long end = std::chrono::duration_cast<std::chrono::UNIT>(std::chrono::system_clock::now().time_since_epoch()).count();
        auto diff = end - start;

        if(diff < neededDelay){
            std::this_thread::sleep_for(std::chrono::UNIT(neededDelay - diff));
        }
        else{
            std::this_thread::sleep_for(std::chrono::UNIT(neededDelay*2 - diff));
        }

        end = std::chrono::duration_cast<std::chrono::UNIT>(std::chrono::system_clock::now().time_since_epoch()).count();
        diff = end - start;

        m_realFps = TIME / diff;
    }


}

void Display::UI::manageEvents() {
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
        m_game.stop();
        return;
    }


    sf::Event event{};
    while(m_window.pollEvent(event)){
        switch(event.type){
            case sf::Event::Resized:
                m_view.setSize(event.size.width, event.size.height);
                break;
            case sf::Event::Closed:
                m_game.stop();
                break;
            case sf::Event::KeyReleased:
                switch (event.key.code){
                    case sf::Keyboard::C:
                        m_game.switchDebug();
                        break;
                }
                break;
            case sf::Event::KeyPressed:
                switch (event.key.code){
                    case sf::Keyboard::E:
                        m_view.rotate(3.0f);
                        break;
                    case sf::Keyboard::A:
                        m_view.rotate(-3.0f);
                        break;
                    case sf::Keyboard::X:
                        m_game.getPlayer().takeDamage(Model::Element::Red);
                        break;
                }
                break;
            default:
                break;
        }
    }

}

