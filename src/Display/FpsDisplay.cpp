#include <sstream>
#include <AndyFind/Display/FpsDisplay.h>
#include <SFML/Graphics/RenderTarget.hpp>

Display::FpsDisplay::FpsDisplay() {
    m_font.loadFromFile("data/fonts/pixelated/pixelated.ttf");
    m_text.setFont(m_font);
    m_text.setCharacterSize(24);
    m_text.setFillColor(sf::Color::Black);
    m_text.setPosition(Vector2(1, 1, true).toPixel().vector2F());
}

void Display::FpsDisplay::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    target.draw(m_text, states);
}

void Display::FpsDisplay::update(Display::UI &ui) {
    ui.resetView();
    std::stringstream str;
    str << static_cast<int>(ui.m_realFps) << " fps";
    m_text.setString(str.str());
}
