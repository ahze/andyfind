#include <sstream>
#include <AndyFind/Display/EntityDebug.h>
#include <AndyFind/Display/UI.h>

Display::EntityDebug::EntityDebug() {
    m_font.loadFromFile("data/fonts/pixelated/pixelated.ttf");
    m_text.setFont(m_font);
    m_text.setCharacterSize(24);
    m_text.setFillColor(sf::Color::Black);
    m_text.setPosition(Vector2(1, 2, true).toPixel().vector2F());
}

void Display::EntityDebug::update(Display::UI &ui) {
    Vector2 mouseWindowPos(sf::Mouse::getPosition(ui.getWindow()), false);
    Vector2 mouseGamePos = (mouseWindowPos + ui.getUpLeftCorner()).toCoordinate();

    //Gamens::Game& game = ui.getGame();
    std::stringstream str;

    Model::Entity::Character* entity = ui.getGame().getCharacterOnBox(mouseGamePos);
    if(entity != nullptr){
        str << *entity;
    }
    else{
        str << "Mouse : " << mouseGamePos.toCoordinate();
    }

    m_text.setString(str.str());

    ui.resetView();
}

void Display::EntityDebug::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    target.draw(m_text, states);
}
