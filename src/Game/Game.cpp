#include <iostream>
#include <AndyFind/Game/Game.h>
#include <AndyFind/Models/Map/MapFactory/BoxArrayFactory.h>
#include <AndyFind/Utils/Util.h>

Gamens::Game::Game() : m_player(Model::Entity::Player(7, 7)),
                                                                          m_map(),
                                                                          m_events(),
                                                                          m_running(true),
                                                                          m_tick(0),
                                                                          m_debug(false){
    std::cout << m_map << std::endl;
    m_enemies.push_back(std::make_unique<Model::Entity::Enemy>(m_player.getPos().m_x, m_player.getPos().m_y, Model::Element::Element::Blue));
}


void Gamens::Game::update(Game &game) {
    m_tick++;
    m_map.update(*this);

    if(!m_events.empty()){
        m_events[0]->update(*this);
        if(m_events[0]->isOver(*this)){
            m_events[0]->whenOver(*this);
            m_events.erase(m_events.begin());
        }
    }
    if(m_events.empty()){
        m_player.update(*this);
        for(const auto& en : m_enemies){
            en->update(*this);
        }
    }
}

void Gamens::Game::stop() {
    m_running = false;
}

void Gamens::Game::switchDebug() {
    m_debug = !m_debug;
    std::cout << (m_debug ? "Now debugging" : "Not debugging anymore") << std::endl;
}

void Gamens::Game::newEvent(std::unique_ptr<Model::Event::Event> &event) {
    m_events.push_back(move(event));
}

bool Gamens::Game::isRunning() const {
    return m_running;
}

Model::Map::Map &Gamens::Game::getMap() {
    return m_map;
}

Model::Entity::Player &Gamens::Game::getPlayer() {
    return m_player;
}

int Gamens::Game::getTick() const{
    return m_tick;
}

std::vector<std::unique_ptr<Model::Entity::Enemy>>& Gamens::Game::getEnemies() {
    return m_enemies;
}

Model::Entity::Character *Gamens::Game::getCharacterOnBox(const Vector2 pos)  {
    std::cout << "player : " << m_player.getPos() << "   " << pos << " " << (m_player.getPos() == pos) << std::endl;
    if(pos == m_player.getPos()) {
        return &m_player;
    }
    for(const auto& en : m_enemies){
        if(en->getPos() == pos){
            return en.get();
        }
    }
    return nullptr;
}

void Gamens::Game::draw(sf::RenderTarget &target, sf::RenderStates states) {
    m_map.draw(target, states);
    for(const auto& en : m_enemies){
        en->draw(target, states);
    }
    m_player.draw(target, states);
}

