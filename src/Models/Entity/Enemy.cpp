#include <AndyFind/Models/Entity/Enemy.h>
#include <AndyFind/Game/Game.h>
#include <SFML/Graphics/CircleShape.hpp>

Model::Entity::Enemy::Enemy(int x, int y, Model::Element::Element mElement) : Character(x, y, 4),
                                                                              m_element(mElement){

}

void Model::Entity::Enemy::update(Gamens::Game &game) {
    if(playerAround(game)){
        //TODO Hit the player
    }
    else{
        //TODO find the player
    }
}


bool Model::Entity::Enemy::playerAround(Gamens::Game &game) {
    for(const auto& v : game.getMap().boxesAround(m_pos)){
        if(game.getPlayer().getPos() == v.first)
            return true;
    }
    return false;
}

void Model::Entity::Enemy::draw(sf::RenderTarget &target, sf::RenderStates states) {
    drawBaseSquare(Model::Element::getRawColor(m_element), sf::Color::Black);
    drawHealthTriangles(Model::Element::getColor(m_element));
    drawDirection(sf::Color::White);
    Character::draw(target, states);
}
