#include <iostream>
#include <SFML/Graphics/RenderWindow.hpp>

#include <AndyFind/Game/Game.h>
#include <AndyFind/Display/UI.h>
#include <AndyFind/Models/Entity/Player.h>
#include <SFML/Graphics/CircleShape.hpp>
#include <AndyFind/Utils/Util.h>

Model::Entity::Player::Player(int x, int y) : Character(x, y, 4), m_wantAttack(false), m_attackDirection(Misc::Direction::None) {

}

void Model::Entity::Player::update(Gamens::Game &game) {

    if(m_wantAttack && !m_controller.isElementPressed(m_attackElement)){
        m_wantAttack = false;
        if(m_attackDirection != Misc::Direction::None){
            attack(game, m_attackDirection, m_attackElement);
            return;
        }
    }

    for(Element::Element e : Element::getAllElements()){
        if(m_controller.isElementPressed(e)){
            m_wantAttack = true;
            m_attackElement = e;
            if(m_attackDirection != Misc::Direction::None)
                m_direction = m_attackDirection;
            break;
        }
    }
    m_attackDirection = Misc::Direction::None;
    for(Direction d : Misc::Direction::allDirections()){
        if(m_controller.isDirectionPressed(d)){
            if(m_wantAttack){
                m_attackDirection = d;
                break;
            }
            else if(game.getMap().canGoThisWay(*this, d)){
                move(game, d);
                break;
            }
        }
    }

}

void Model::Entity::Player::draw(sf::RenderTarget &target, sf::RenderStates states) {
    drawBaseSquare(sf::Color::Black, sf::Color(70, 70, 70));
    drawHealthTriangles(sf::Color(190, 190, 190));
    drawDirection(sf::Color::Yellow);
    if(m_wantAttack)
        drawAttacks();
    else if(m_vertexArrays.find("attacks") != m_vertexArrays.end()){
        m_vertexArrays.erase("attacks");
    }

    Character::draw(target, states);
}

void Model::Entity::Player::drawAttacks() {
    auto& va = getAndCheckVA("attacks", sf::Quads, 4*4*2);
    int i = 0;
    sf::Color inColor = Element::getColor(m_attackElement);
    sf::Color outColor = Element::getRawColor(m_attackElement);
    for(const auto& direction : Misc::Direction::allDirections()){
        outColor.a = direction == m_attackDirection ? 150 : 80;
        inColor.a = direction == m_attackDirection ? 150 : 80;

        Vector2 pos = m_pos + direction.toVector();
        makeSquare(va, pos, Display::sideLength, outColor, i);
        makeSquare(va, pos.toPixel()+m_outlineThickness, Display::sideLength-m_outlineThickness*2, inColor, i);
    }

}


