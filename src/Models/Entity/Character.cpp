#include <memory>
#include <iostream>
#include <string>
#include <AndyFind/Models/Entity/Character.h>
#include <AndyFind/Models/Event/MoveEvent.h>
#include <AndyFind/Game/Game.h>
#include <AndyFind/Models/Event/AttackEvent.h>
#include <AndyFind/Display/UI.h>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>

Model::Entity::Character::Character(int x, int y, int outlineThickness) : Entity(x, y),
                                                                          m_hp(8),
                                                                          m_direction(Misc::Direction::Right),
                                                                          m_vertexArrays(),
                                                                          m_outlineThickness(outlineThickness){

}

void Model::Entity::Character::move(Gamens::Game &game, Misc::Direction dir) {
    m_direction = dir;
    if(game.getMap().canGoThisWay(*this, dir)){
        std::unique_ptr<Model::Event::Event> n(new Event::MoveEvent(game, *this, dir));
        game.newEvent(n);
    }
}

void Model::Entity::Character::attack(Gamens::Game &game, Misc::Direction direction, Model::Element::Element element) {
    Vector2 attackPos = m_pos + direction.toVector();
    if(game.getMap().isInBound(attackPos)){
        std::unique_ptr<Event::Event> event(new Event::AttackEvent(game, *this, direction, element));
        game.newEvent(event);
    }
}

void Model::Entity::Character::takeDamage(Model::Entity::Character &source, Model::Element::Element element) {
    m_hp--;
}

void Model::Entity::Character::takeDamage(Model::Element::Element element) {
    m_hp--;
}


Misc::Direction Model::Entity::Character::getDirection() const {
    return m_direction;
}


//region displaying

void Model::Entity::Character::draw(sf::RenderTarget &target, sf::RenderStates states) {
    for(const auto& [name, va] : m_vertexArrays) {
        target.draw(va);
    }
}

sf::VertexArray &Model::Entity::Character::getAndCheckVA(const std::string& name, const sf::PrimitiveType type, const int vertexCount) {
    if(m_vertexArrays.find(name) == m_vertexArrays.end()){
        m_vertexArrays[name] = sf::VertexArray(type, vertexCount);
    }
    return m_vertexArrays[name];
}

void Model::Entity::Character::makeSquare(sf::VertexArray &va, Vector2 pos, int sideSize, const sf::Color color, int& startIndex) {
    makeRectangle(va, pos, Vector2(sideSize, sideSize), color, startIndex);
}

void Model::Entity::Character::makeSquare(sf::VertexArray &va, Vector2 pos, int sideSize, sf::Color color) {
    int i = 0;
    makeSquare(va, pos, sideSize, color, i);
}

void Model::Entity::Character::makeRectangle(sf::VertexArray &va, Vector2 pos, Vector2 size, const sf::Color color, int& startIndex) {
    if(va.getPrimitiveType() != sf::Quads)
        return;

    pos = pos.toPixel();
    va[startIndex].position = pos.vector2F();
    va[startIndex++].color = color;


    for(const Direction d : {Direction::Right, Direction::Down, Direction::Left}){
        auto v = d.toVector();
        v.m_isCoordinate = false;
        pos += v * size;
        va[startIndex].position = pos.vector2F();
        va[startIndex++].color = color;
    }

}

void Model::Entity::Character::makeRectangle(sf::VertexArray &va, Vector2 pos, Vector2 size, const sf::Color color) {
    int i = 0;
    makeRectangle(va, pos, size, color, i);
}

void Model::Entity::Character::drawBaseSquare(sf::Color out, sf::Color in) {
    auto& va = getAndCheckVA("base", sf::Quads, 8);
    int i = 0;
    makeSquare(va, m_pos.toPixel() + m_drawDiff, Display::sideLength, out, i);
    makeSquare(va, m_pos.toPixel() + m_drawDiff + m_outlineThickness, Display::sideLength - m_outlineThickness*2, in, i);
}

void Model::Entity::Character::drawHealthTriangles(sf::Color color) {
    auto &va = getAndCheckVA("hpfans", sf::TrianglesFan, m_hp+2);
    va.resize(m_hp+2);
    int innerHalfside = Display::halfSide - m_outlineThickness;

    Vector2 mid = m_pos.toPixel() + (Display::sideLength/2);
    Vector2 start = m_pos.toPixel() + Vector2(Display::halfSide, m_outlineThickness, false);
    std::vector<Vector2> steps{
            Vector2(innerHalfside, 0, false),
            Vector2(0, innerHalfside, false),
            Vector2(0, innerHalfside, false),
            Vector2(-innerHalfside, 0, false),
            Vector2(-innerHalfside, 0, false),
            Vector2(0, -innerHalfside, false),
            Vector2(0, -innerHalfside, false),
            Vector2(innerHalfside, 0, false)
    };

    va[0].position = (mid + m_drawDiff).vector2F();
    va[0].color = color;
    for(int i = 0; i <= m_hp +1; i++){
        va[i+1].position = (start + m_drawDiff).vector2F();
        va[i+1].color = color;
        start = start + steps[i%8];
    }

}

void Model::Entity::Character::drawDirection(sf::Color color) {
    auto& va = getAndCheckVA("direction", sf::Quads, 4);
    Vector2 pos = m_pos.toPixel();

    if (m_direction.toVector().m_x == 1) {
        pos.m_x += Display::sideLength - m_outlineThickness;
    }
    if (m_direction.toVector().m_y == 1) {
        pos.m_y += Display::sideLength - m_outlineThickness;
    }

    Vector2 size(Display::sideLength, m_outlineThickness);
    if(m_direction.toVector().m_x != 0){
        int tmp = size.m_x;
        size.m_x = size.m_y;
        size.m_y = tmp;
    }

    makeRectangle(va, pos + m_drawDiff, size, color);
}

std::ostream &Model::Entity::operator<<(std::ostream &os, const Model::Entity::Character &character) {
    os << static_cast<const Entity &>(character) << " hp: " << character.m_hp << " direction: "
       << character.m_direction;
    return os;
}

//endregion