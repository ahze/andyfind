#include <iostream>
#include <string>
#include <sstream>

#include <AndyFind/Models/Entity/Entity.h>
#include <AndyFind/Game/Game.h>

Model::Entity::Entity:: Entity(int x, int y) : m_pos(x, y),
                                              m_drawDiff(0, 0, false){

}

Vector2& Model::Entity::Entity::getDrawDiff() {
    return m_drawDiff;
}

const Vector2 Model::Entity::Entity::getPos() const {
    return m_pos;
}


void Model::Entity::Entity::setDrawDiff(Vector2 &v){
    m_drawDiff = v;
}

void Model::Entity::Entity::setPos(Vector2 &v){
    m_pos = v;
}

std::ostream &Model::Entity::operator<<(std::ostream &os, const Model::Entity::Entity &entity) {
    os << "Entity" << entity.getPos();
    return os;
}
