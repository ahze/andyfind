#include <utility>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

#include <SFML/Graphics/RectangleShape.hpp>

#include <AndyFind/Models/Map/Map.h>
#include <AndyFind/Models/Map/Boxes/Box.h>
#include <AndyFind/Models/Map/Boxes/Ground.h>
#include <AndyFind/Game/Game.h>
#include <AndyFind/Utils/Util.h>
#include <AndyFind/Models/Map/MapFactory/SimpleMapFactory.h>
#include <AndyFind/Display/UI.h>


Model::Map::Map::Map() : m_boxes(),
                                                             m_size(0, 0, true),
                                                             m_wallColor(0, 0, 0, 100) {
    std::unique_ptr<MapFactory::BoxArrayFactory> factory = std::make_unique<MapFactory::BoxArrayFactory>();
    factory->generateBoxes(m_boxes);
    m_size.set(m_boxes[m_boxes.size() - 1]->getPos() + 1);

    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(m_boxes.size() * 4);

    // on remplit le tableau de vertex, avec un quad par tuile

    for (int j = 0; j < m_size.m_y; ++j) {
        for (int i = 0; i < m_size.m_x; ++i) {

            // on récupère un pointeur vers le quad à définir dans le tableau de vertex
            int tileNumber = i + j * m_size.m_x;
            sf::Vertex *quad = &m_vertices[tileNumber * 4];

            // on définit ses quatre coins
            quad[0].position = m_boxes[tileNumber]->getPos().toPixel().vector2F();
            quad[1].position = (m_boxes[tileNumber]->getPos() + Vector2(1, 0)).toPixel().vector2F();
            quad[2].position = (m_boxes[tileNumber]->getPos() + Vector2(1, 1)).toPixel().vector2F();
            quad[3].position = (m_boxes[tileNumber]->getPos() + Vector2(0, 1)).toPixel().vector2F();

            for (int q = 0; q < 4; q++) {
                sf::Color c = Element::getColor(m_boxes[tileNumber]->getElement());
                if (m_boxes[tileNumber]->isWall()) {
                    c.r -= 50;
                    c.g -= 50;
                    c.b -= 50;
                }
                int diff = Utils::random(0, 40);
                switch (Utils::random(1, 4)) {
                    case 1:
                        c.r -= diff;
                        break;
                    case 2:
                        c.g -= diff;
                        break;
                    case 3:
                        c.b -= diff;
                        break;
                }
                quad[q].color = c;
            }


            if (m_boxes[tileNumber]->isWall()) {
                // Vector2 pos = m_boxes[tileNumber]->getPos();
                for (const auto &nextTo : boxesAround(m_boxes[tileNumber]->getPos())) {
                    auto &room = getBox(nextTo.first);
                    if (!room->isWall()) {
                        for (int q = 0; q < 2; q++) {
                            auto &color = quad[(nextTo.second.value() + q) % 4].color;
                            color.r = 80;
                            color.g = 80;
                            color.b = 80;
                        }
                    }
                }
            }
        }
    }
}

void Model::Map::Map::draw(sf::RenderTarget &target, sf::RenderStates states) {

    target.draw(m_vertices);
    target.draw(&m_lines[0], m_lines.size(), sf::Lines);

}


void Model::Map::Map::update(Gamens::Game &game) {
    m_wallColor.a = 200 + std::cos(game.getTick()/(Display::fps*2))*20;
}

std::ostream& Model::Map::operator<<(std::ostream &strm, const Model::Map::Map &m) {
    int y = 0;
    strm << "0\t";
    for(const auto &b : m.m_boxes){
        if(b->getPos().m_y != y){
            y = b->getPos().m_y;
            strm << std::endl << y << "\t";
        }
        strm << b->getChar() << ' ';
    }
    return strm << std::endl;
}

std::unique_ptr<Box> &Model::Map::Map::getBox(const Vector2& v) {
    if(!isInBound(v)){
        std::stringstream str;
        str << "Out of map's bound " << v << std::endl << "Map size : " << m_size << std::endl;
        throw std::invalid_argument(str.str());
    }
    return m_boxes[VectorToInt(v)];
}


bool Model::Map::Map::canGoThisWay(const Model::Entity::Character &c, Misc::Direction dir) {
    Vector2 newPos = c.getPos() + dir.toVector();
    if(isInBound(newPos)){
        return getBox(newPos)->canStepOn();
    }
    return false;
}

bool Model::Map::Map::isInBound(const Vector2& v) const{
    return v.m_x >= 0 && v.m_x < m_size.m_x && v.m_y >= 0 && v.m_y < m_size.m_y;
}

int Model::Map::Map::VectorToInt(const Vector2 &v) const {
    Vector2 v2 = v.toCoordinate();
    return v2.m_x + v2.m_y*m_size.m_x;
}

const sf::Color &Model::Map::Map::getWallColor() const {
    return m_wallColor;
}

const Vector2 &Model::Map::Map::getSize() const {
    return m_size;
}

std::vector<std::pair<Vector2, Direction>> Model::Map::Map::boxesAround(const Vector2 &v) const {
    auto res = std::vector<std::pair<Vector2, Direction>>();
    for(const auto& d : Misc::Direction::allDirections()){
        Vector2 nextTo = v + d.toVector();
        if(isInBound(nextTo))
            res.push_back(std::pair<Vector2, Direction>(nextTo, d));
    }
    return res;
}


