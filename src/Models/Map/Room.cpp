#include "AndyFind/Models/Map/Room.h"

Model::Map::Room::Room(Vector2 &mUpLeftCorner, Vector2 &mSize, Model::Element::Element mElement) : m_upLeftCorner(mUpLeftCorner),
                                                                                                   m_size(mSize),
                                                                                                   m_element(mElement) {}


const Vector2 &Model::Map::Room::getUpLeftCorner() const {
    return m_upLeftCorner;
}

const Vector2 &Model::Map::Room::getSize() const {
    return m_size;
}

Model::Element::Element Model::Map::Room::getElement() const {
    return m_element;
}
