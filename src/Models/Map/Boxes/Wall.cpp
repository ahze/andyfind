#include <AndyFind/Models/Map/Boxes/Wall.h>
#include <AndyFind/Game/Game.h>
#include <iostream>
#include <AndyFind/Utils/Util.h>


Model::Map::Boxes::Wall::Wall(int x, int y, Model::Element::Element e) : Box(x, y, e) {

}

bool Model::Map::Boxes::Wall::canStepOn() {
    return false;
}

bool Model::Map::Boxes::Wall::activateEffect(Model::Entity::Character &character) {
    return false;
}



void Model::Map::Boxes::Wall::update(Gamens::Game &game) {

}

char Model::Map::Boxes::Wall::getChar() {
    return 'X';
}


bool Wall::isWall() const {
    return true;
}
