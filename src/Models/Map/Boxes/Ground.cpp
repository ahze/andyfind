#include <AndyFind/Models/Map/Boxes/Ground.h>
#include <SFML/Graphics/RenderTarget.hpp>

Model::Map::Boxes::Ground::Ground(int x, int y, Model::Element::Element e) : Box(x, y, e) {

}

bool Model::Map::Boxes::Ground::canStepOn() {
    return true;
}

bool Model::Map::Boxes::Ground::activateEffect(Model::Entity::Character &character) {
    return false;
}

void Model::Map::Boxes::Ground::update(Gamens::Game &game) {

}

char Model::Map::Boxes::Ground::getChar() {
    return ' ';
}
