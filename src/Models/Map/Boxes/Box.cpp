#include <AndyFind/Models/Map/Boxes/Box.h>
#include <AndyFind/Game/Game.h>

Model::Map::Boxes::Box::Box(int x, int y, Model::Element::Element element) : Entity(x, y),
                                                                             m_element(element){
}

Model::Element::Element Box::getElement() const {
    return m_element;
}

bool Box::isWall() const {
    return false;
}
