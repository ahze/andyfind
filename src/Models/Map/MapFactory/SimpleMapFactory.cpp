#include <AndyFind/Models/Map/MapFactory/SimpleMapFactory.h>
#include <AndyFind/Utils/Util.h>
#include <AndyFind/Models/Map/Boxes/Wall.h>
#include <AndyFind/Models/Map/Boxes/Ground.h>

Model::Map::MapFactory::SimpleMapFactory::SimpleMapFactory() : BoxArrayFactory() {

}

void Model::Map::MapFactory::SimpleMapFactory::generateBoxes(std::vector<std::unique_ptr<Model::Map::Boxes::Box>> &boxes) {
    int size = Utils::random(20, 50);
    Element::Element e = static_cast<Element::Element>(Utils::random(0, 3));
    for(int y = 0; y < size; y++){
        for(int x = 0; x < size; x++){
            if(x == 0 || y == 0 || x == size - 1 || y == size - 1){
                boxes.push_back(std::make_unique<Wall>(x, y, e));
            }
            else{
                boxes.push_back(std::make_unique<Ground>(x, y, e));
            }
        }
    }
}
