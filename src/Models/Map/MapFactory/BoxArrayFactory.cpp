#include <iostream>
#include <AndyFind/Models/Map/MapFactory/BoxArrayFactory.h>
#include <AndyFind/Models/Map/Boxes/Wall.h>
#include <AndyFind/Models/Map/Boxes/Ground.h>
#include <AndyFind/Utils/Util.h>
#include <AndyFind/Models/Map/MapFactory/CorridorDigger.h>

using namespace Model::Map::Boxes;


void Model::Map::MapFactory::BoxArrayFactory::generateBoxes(std::vector<std::unique_ptr<Model::Map::Boxes::Box>>& boxes) {

    int rawCount =  Utils::random(4, 6),
    lineCount = Utils::random(6, 7);
    m_roomCount= rawCount*lineCount;

    std::vector<Element::Element> elements;

    while (m_rooms.size() < m_roomCount){
        int maxHeight = getMaxHeight();
        sf::RectangleShape lastRoom = getRandomRoom();
        int y = maxHeight + Utils::random(3, 6);
        int x = Utils::random(2, 5);
        lastRoom.setPosition(x, y);
        m_rooms.push_back(lastRoom);
        elements.push_back(static_cast<Element::Element>(Utils::random(0, 3)));


        for(int i = 0; i < rawCount - 1; i++){
            auto r = getRandomRoom();
            y = maxHeight + Utils::random(8, 13);
            x = lastRoom.getPosition().x + lastRoom.getSize().x + Utils::random(7, 12);
            r.setPosition(x, y);
            m_rooms.push_back(r);
            elements.push_back(static_cast<Element::Element>(Utils::random(0, 3)));
            lastRoom = r;
        }
    }


    CorridorDigger corridorDigger = CorridorDigger(Vector2(rawCount, lineCount, true));
    corridorDigger.dig(m_rooms);

    int mapWidth = getMaxWidth() + Utils::random(3, 6);
    int mapHeight = getMaxHeight() + Utils::random(3, 6);


    for(int y = 0; y  < mapHeight; y++) {
        for(int x = 0; x  < mapWidth; x++){
            bool isInElement = false;
            for(int i = 0; i < m_rooms.size(); i++){
                if(m_rooms[i].getGlobalBounds().contains(x, y)){
                    isInElement = true;
                    break;
                }
            }
            int roomNumber = getNearestRoom(Vector2(x, y));
            auto e = elements[roomNumber];
            if(isInElement)
                boxes.push_back(std::make_unique<Ground>(x, y, e));
            else
                boxes.push_back(std::make_unique<Wall>(x, y, e));
        }
    }
}

int Model::Map::MapFactory::BoxArrayFactory::getMaxWidth() const{
    int max = 0;
    for(const auto& room : m_rooms){
        int x  = (int)room.getPosition().x + (int)room.getSize().x;
        if(x > max)
            max = x;
    }
    return max;
}

int Model::Map::MapFactory::BoxArrayFactory::getMaxHeight() const {
    int max = 0;
    for(const auto& room : m_rooms){
        int y  = (int)room.getPosition().y + (int)room.getSize().y;
        if(y > max)
            max = y;
    }
    return max;
}

sf::RectangleShape Model::Map::MapFactory::BoxArrayFactory::getRandomRoom() {
    int width = Utils::random(5, 12);
    int height = Utils::random(5, 12);
    auto r = sf::RectangleShape(sf::Vector2f(width, height));
    return r;
}

int Model::Map::MapFactory::BoxArrayFactory::getNearestRoom(Vector2 box) const {
    int indexMinRoom = 0;
    Vector2 center = Vector2(m_rooms[0].getPosition().x + m_rooms[0].getSize().x/2, m_rooms[0].getPosition().y + m_rooms[0].getSize().y/2);
    double distanceMin = center.distanceTo(box);
    for(int i = 1; i < m_roomCount; i++){
        center = Vector2(m_rooms[i].getPosition().x + m_rooms[i].getSize().x/2, m_rooms[i].getPosition().y + m_rooms[i].getSize().y/2);
        double distance = center.distanceTo(box);
        if(distance < distanceMin){
            distanceMin = distance;
            indexMinRoom = i;
        }
    }
    return indexMinRoom;
}

