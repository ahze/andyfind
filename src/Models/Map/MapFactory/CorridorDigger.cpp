#include <AndyFind/Models/Map/MapFactory/CorridorDigger.h>
#include <AndyFind/Utils/Util.h>
#include <iostream>

Model::Map::MapFactory::CorridorDigger::CorridorDigger(Vector2 size) : m_size(size){
}

/**
 *  Loops on all the corridors and dig them
 * @param rooms
 * @return
 */
void Model::Map::MapFactory::CorridorDigger::dig(std::vector<sf::RectangleShape>& rooms) {
    for(int i = 0; i < corridorNumber(); i++){
        digOneCorridor(getRoomPair(rooms, i), rooms, isHorizontal(i));
    }
}

/**
 *  Will dig the corridor between the two rooms in the pair
 *  To do so, it adds 1 by 1 rectangles in the vector
 * @param rooms
 * @param corridors
 * @param horizontal
 */
void Model::Map::MapFactory::CorridorDigger::digOneCorridor(std::pair<sf::RectangleShape, sf::RectangleShape> rooms,
                                                            std::vector<sf::RectangleShape> &corridors,
                                                            bool horizontal) {
    Vector2 actualPos(0, 0, true);
    Vector2 target(0, 0, true);
    if(horizontal){
        actualPos.m_x = rooms.first.getPosition().x + rooms.first.getSize().x-1;
        actualPos.m_y = rooms.first.getPosition().y + Utils::random(0, rooms.first.getSize().y);

        target.m_x = rooms.second.getPosition().x+1;
        target.m_y = rooms.second.getPosition().y + Utils::random(0, rooms.second.getSize().y);
    }
    else{
        actualPos.m_x = rooms.first.getPosition().x + Utils::random(0, rooms.first.getSize().x);
        actualPos.m_y = rooms.first.getPosition().y + rooms.first.getSize().y-1;

        target.m_x = rooms.second.getPosition().x + Utils::random(0, rooms.second.getSize().x);
        target.m_y = rooms.second.getPosition().y+1;
    }

    while(actualPos != target){
        auto r = sf::RectangleShape(sf::Vector2f(1, 1));
        if(actualPos.m_x == target.m_x){
            actualPos.m_y -= Utils::compareIntegers(actualPos.m_y, target.m_y);
        }
        else if(actualPos.m_y == target.m_y){
            actualPos.m_x -= Utils::compareIntegers(actualPos.m_x, target.m_x);
        }
        else{
            if(Utils::random(1, 4) > 1){
                actualPos.m_x -= Utils::compareIntegers(actualPos.m_x, target.m_x);
            }
            else{
                actualPos.m_y -= Utils::compareIntegers(actualPos.m_y, target.m_y);
            }
        }

        r.setPosition(actualPos.m_x, actualPos.m_y);
        corridors.push_back(r);
    }
}

/**
 * Returns a pair a rooms, which are the ones linked by the corridor numbered i
 */
std::pair<sf::RectangleShape, sf::RectangleShape> Model::Map::MapFactory::CorridorDigger::getRoomPair(std::vector<sf::RectangleShape> &rooms,
                                                                                                      int i) {
    auto res = std::pair<sf::RectangleShape, sf::RectangleShape>();
    if(isHorizontal(i)){
        i += i / (m_size.m_x - 1);
        res.first = rooms[i];
        res.second = rooms[i+1];
    }
    else{
        i -= (m_size.m_x - 1)*m_size.m_y;
        res.first = rooms[i];
        res.second = rooms[i + m_size.m_x];
    }

    return res;
}

/**
 * Tells if the corridor is horizontal or not depending on its number
 */
bool Model::Map::MapFactory::CorridorDigger::isHorizontal(int i) {
    return i < (m_size.m_x - 1)*m_size.m_y;
}

int Model::Map::MapFactory::CorridorDigger::corridorNumber() const {
    return (m_size.m_x - 1)*m_size.m_y + m_size.m_x*(m_size.m_y - 1);
}


