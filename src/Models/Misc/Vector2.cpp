#include <iostream>
#include <math.h>
#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Display/UI.h>


Vector2::Vector2(int x, int y, bool isCoordinate) : m_x(x), m_y(y), m_isCoordinate(isCoordinate) {
}

Vector2::Vector2(sf::Vector2f v, bool isCoordinate) : Vector2(v.x, v.y, isCoordinate) {

}



void Vector2::set(int x, int y) {
    m_x = x;
    m_y = y;
}

Vector2 Vector2::toPixel() const{
    return m_isCoordinate ? Vector2(m_x*Display::sideLength, m_y*Display::sideLength, false) : *this;
}

Vector2 Vector2::toCoordinate() const{
    return m_isCoordinate ? *this : Vector2(m_x/Display::sideLength, m_y/Display::sideLength, true);
}

std::ostream &operator<<(std::ostream &os, const Vector2 &v) {
    os << "{x=" << v.m_x << ", y=" << v.m_y << ", " << (v.m_isCoordinate ? "Coord" : "Pixel") << "}";
    return os;
}

sf::Vector2f Vector2::vector2F() const {
    return sf::Vector2f(m_x, m_y);
}

void Vector2::set(const Vector2 &v) {
    m_x = v.m_x;
    m_y = v.m_y;
    m_isCoordinate = v.m_isCoordinate;
}

Vector2 &Vector2::operator+=(const Vector2 &r) {
    Vector2 right = m_isCoordinate ? r.toCoordinate() : r.toPixel();
    m_x += right.m_x;
    m_y += right.m_y;
    return *this;
}

bool Vector2::isOrigin() const {
    return m_x == 0 && m_y == 0;
}

double Vector2::distanceTo(const Vector2 &v) const {

    return sqrt((m_x - v.m_x)*(m_x - v.m_x) + (m_y - v.m_y)*(m_y - v.m_y));
}

Vector2 operator+(const Vector2 &v1, const Vector2 &v2) {
    if(v1.m_isCoordinate == v2.m_isCoordinate)
        return {v1.m_x + v2.m_x, v1.m_y + v2.m_y, v1.m_isCoordinate};
    else if(v1.m_isCoordinate){
        return Vector2(v1.m_x + v2.toCoordinate().m_x, v1.m_y + v2.toCoordinate().m_y, true);
    }
    else{
        return Vector2(v1.m_x + v2.toPixel().m_x, v1.m_y + v2.toPixel().m_y, false);
    }
}



Vector2 operator-(const Vector2 &v){
    return {-v.m_x, -v.m_y, v.m_isCoordinate};
}



Vector2 operator-(const Vector2 &v1, const Vector2 &v2) {
    if(v1.m_isCoordinate == v2.m_isCoordinate)
        return {v1.m_x - v2.m_x, v1.m_y - v2.m_y, v1.m_isCoordinate};
    else if(v1.m_isCoordinate){
        return Vector2(v1.m_x - v2.toCoordinate().m_x, v1.m_y - v2.toCoordinate().m_y, true);
    }
    else{
        return Vector2(v1.m_x - v2.toPixel().m_x, v1.m_y - v2.toPixel().m_y, false);
    }
}

bool operator==(const Vector2 &v1, const Vector2 &v2) {
    if(v1.m_isCoordinate == v2.m_isCoordinate)
        return v1.m_x == v2.m_x && v1.m_y == v2.m_y;
    else if(v1.m_isCoordinate){
        return v1.m_x == v2.toCoordinate().m_x &&  v1.m_y == v2.toCoordinate().m_y;
    }
    else{
        return v1.m_x == v2.toPixel().m_x && v1.m_y == v2.toPixel().m_y;
    }
}

bool operator!=(const Vector2 &v1, const Vector2 &v2) {
    return !(v1 == v2);
}

Vector2 operator+(const Vector2 &left, const Direction &right) {
    return left + right.toVector();
}

Vector2 operator*(const Vector2 &left, const Vector2 &right) {
    return {left.m_x * right.m_x, left.m_y * right.m_y, left.m_isCoordinate};
}

