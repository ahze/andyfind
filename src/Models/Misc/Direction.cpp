#include <vector>
#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Models/Misc/Direction.h>


Vector2 Misc::Direction::toVector() const{
    static const std::vector<Vector2> vectors({
                                                      Vector2(0, -1),
                                                      Vector2(1, 0),
                                                      Vector2(0, 1),
                                                      Vector2(-1, 0),
                                                      Vector2(0, 0)
                                              });
    return vectors[m_value];
}

std::vector<Misc::Direction> Misc::Direction::allDirections() {
    return std::vector<Direction>({
                                          Misc::Direction::Up,
                                          Misc::Direction::Right,
                                          Misc::Direction::Down,
                                          Misc::Direction::Left
                                  });
}

int Misc::Direction::value() const {
    return m_value;
}

std::ostream &Misc::operator<<(std::ostream &str, const Misc::Direction &direction) {
    switch (direction) {
        case Direction::None:
            str << "None";
            break;
        case Direction::Up:
            str << "Up";
            break;
        case Direction::Right:
            str << "Right";
            break;
        case Direction::Down:
            str << "Down";
            break;
        case Direction::Left:
            str << "Left";
            break;
    }
    return str;
}
