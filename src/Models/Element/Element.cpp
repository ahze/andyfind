#include <AndyFind/Models/Element/Element.h>
#include <iostream>
#include <vector>

sf::Color Model::Element::getColor(Model::Element::Element e) {
    return colors[e];
}

sf::Color Model::Element::getRawColor(Model::Element::Element e) {
    const static sf::Color rawColors[] = {sf::Color(255, 40, 40),
                                          sf::Color(40, 255, 40),
                                          sf::Color(40, 40, 255)};
    return rawColors[e];
}

const std::vector<Model::Element::Element> Model::Element::getAllElements() {
    auto v = std::vector<Element>();
    v.push_back(Red);
    v.push_back(Green);
    v.push_back(Blue);
    return v;
}

std::ostream &operator<<(std::ostream &str, const Model::Element::Element &element) {
    switch(element){
        case Model::Element::Red:
            return str << "Red";
        case Model::Element::Green:
            return str << "Green";
        case Model::Element::Blue:
            return str << "Blue";
    }
    return str;
}
