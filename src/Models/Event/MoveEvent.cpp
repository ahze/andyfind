#include <iostream>
#include <AndyFind/Models/Event/MoveEvent.h>
#include <AndyFind/Game/Game.h>
#include <AndyFind/Display/UI.h>
#include <AndyFind/Models/Misc/Direction.h>

Model::Event::MoveEvent::MoveEvent(Gamens::Game &game, Model::Entity::Character &c, Misc::Direction dir) : Event(game, 0.1f), m_character(c), m_direction(dir) {

}

void Model::Event::MoveEvent::whenOver(Gamens::Game &game) {
    Vector2 v = m_character.getPos() + m_direction.toVector();
    m_character.setPos(v);
    Vector2 n(0, 0, false);
    m_character.setDrawDiff(n);
}

void Model::Event::MoveEvent::update(Gamens::Game &game) {
    float percent = getPercentage();

    auto v = m_direction.toVector();
    float shift = Display::sideLength * percent;
    auto v2 = v * shift;
    v2.m_isCoordinate = false;
    m_character.getDrawDiff().set(v2);
    Event::update(game);
}
