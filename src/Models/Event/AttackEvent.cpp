#include <AndyFind/Models/Event/AttackEvent.h>
#include <AndyFind/Game/Game.h>
#include <iostream>

Model::Event::AttackEvent::AttackEvent(Gamens::Game &game, Model::Entity::Character &source,
                                       Misc::Direction direction, Element::Element element) : Event(20),
                                                                                              m_source(source),
                                                                                              m_direction(direction),
                                                                                              m_element(element){

    m_target = game.getCharacterOnBox(m_source.getPos() + m_direction.toVector());
}

void Model::Event::AttackEvent::update(Gamens::Game &game) {
    int tick_number = game.getTick() - m_firstTick;
    Event::update(game);
    if(m_target != nullptr){
        auto& drawDiff = m_target->getDrawDiff();
        auto v = m_target->getDirection().toVector() * 2;
        auto v2 = Vector2(v.m_x, v.m_y, false);
        if(tick_number %8 == 0){
            drawDiff.set( drawDiff + v2);
        }
        else if(tick_number %4 == 0){
            drawDiff.set(drawDiff - v2);
        }
    }
}

void Model::Event::AttackEvent::whenOver(Gamens::Game &game) {
    if(m_target != nullptr){
        m_target->takeDamage(m_source, m_element);
        m_target->getDrawDiff().set(0, 0);
    }
    std::cout << "Attack " << m_direction << " with " << m_element << std::endl;
}
