#include <AndyFind/Models/Event/Event.h>
#include <AndyFind/Game/Game.h>
#include <iostream>
#include <AndyFind/Display/UI.h>

Model::Event::Event::Event(int frameNecessary) : m_framesNecessary(frameNecessary){

}


Model::Event::Event::Event(Gamens::Game &game, float duration) : m_framesNecessary(duration * Display::fps) {
    m_firstTick = game.getTick();
}

void Model::Event::Event::update(Gamens::Game &game) {
    m_framesPassed++;
}

bool Model::Event::Event::isOver(const Gamens::Game &game) const {
    return m_framesPassed >= m_framesNecessary;
}

int Model::Event::Event::getFramesNecessary() const {
    return m_framesNecessary;
}

float Model::Event::Event::getPercentage() const {
    return static_cast<float>(m_framesPassed) / static_cast<float>(m_framesNecessary);
}
