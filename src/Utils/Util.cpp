#include <AndyFind/Utils/Util.h>

#include <cstdlib>
#include <chrono>
#include <thread>
#include <unistd.h>

int Utils::random(int minIncluded, int maxExcluded) {
    if(!srandDone){
        srand(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
        srandDone = true;
    }
    return rand() % (maxExcluded - minIncluded) + minIncluded;
}

bool Utils::randomBool() {
    if(!srandDone){
        srand(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
        srandDone = true;
    }
    return random(1, 3) == 1;
}


int Utils::compareIntegers(int x, int y) {
    if(x > y)
        return 1;
    else if(x < y)
        return -1;
    return 0;
}
