#ifndef ANDYFIND_UTIL_H
#define ANDYFIND_UTIL_H


#include <SFML/Graphics/RectangleShape.hpp>

namespace Utils{

    static bool srandDone = false;

    int random(int min, int max);

    bool randomBool();

    int compareIntegers(int x, int y);

}


#endif //ANDYFIND_UTIL_H
