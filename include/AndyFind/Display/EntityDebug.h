#ifndef ANDYFIND_ENTITYDEBUG_H
#define ANDYFIND_ENTITYDEBUG_H

#include <AndyFind/Display/DisplayComponent.h>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

namespace Display{
    class EntityDebug : public DisplayComponent{
    private:
        sf::Font m_font;
        sf::Text m_text;

    protected:
        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    public:
        EntityDebug();
        void update(UI &ui) override;

    };

}

#endif //ANDYFIND_ENTITYDEBUG_H
