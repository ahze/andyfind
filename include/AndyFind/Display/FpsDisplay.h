#ifndef ANDYFIND_FPSDISPLAY_H
#define ANDYFIND_FPSDISPLAY_H


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include <AndyFind/Display/UI.h>
#include <AndyFind/Display/DisplayComponent.h>

namespace Display {
    class FpsDisplay : public DisplayComponent {
    private:
        sf::Text m_text;
        sf::Font m_font;
    protected:
        void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    public:
        FpsDisplay();
        void update(UI& ui) override ;
    };
}

#endif //ANDYFIND_FPSDISPLAY_H
