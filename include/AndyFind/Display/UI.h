#ifndef ANDYFIND_UI_H
#define ANDYFIND_UI_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Game/Game.h>
#include <AndyFind/Display/DisplayComponent.h>

namespace Display {

    const static int sideLength = 40, halfSide = sideLength / 2, fps = 60;

    class UI{
    private:
        Gamens::Game m_game;
        Model::Entity::Entity& m_cameraTarget;
        Vector2 m_upLeftCorner;

        sf::RenderWindow m_window;
        sf::View m_view;
        std::vector<std::unique_ptr<DisplayComponent>> m_additionalComponents;
        void updateView();
    public:
        float m_realFps;

        //functionning
        void start();
        void manageEvents();

        //displaying
        UI(int width, int height);
        void display(Gamens::Game &game);
        void setViewOnTarget();
        void resetView();

        Gamens::Game& getGame(){ return m_game;}
        sf::RenderWindow& getWindow() { return m_window; }
        sf::View& getView() { return m_view; }
        Vector2 getUpLeftCorner() { return m_upLeftCorner;}
    };

}

#endif