#ifndef ANDYFIND_DISPLAYCOMPONENT_H
#define ANDYFIND_DISPLAYCOMPONENT_H


#include <SFML/Graphics/Drawable.hpp>

namespace Display {
    class UI;

    class DisplayComponent : public sf::Drawable {

    public:
        virtual void update(UI& ui) = 0;
    };
}

#endif //ANDYFIND_DISPLAYCOMPONENT_H
