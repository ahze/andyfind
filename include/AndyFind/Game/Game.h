#ifndef ANDYFIND_GAME_H
#define ANDYFIND_GAME_H


#include <AndyFind/Models/Misc/Updatable.h>
#include <AndyFind/Models/Map/Map.h>
#include <AndyFind/Models/Entity/Player.h>
#include <AndyFind/Models/Event/Event.h>
#include <AndyFind/Models/Entity/Enemy.h>
#include <AndyFind/Models/Map/MapFactory/BoxArrayFactory.h>

namespace Gamens {

    class Game : public Model::Updatable{
    private:
        bool m_running, m_debug;
        int m_tick;
        Model::Map::Map m_map;
        Model::Entity::Player m_player;

        std::vector<std::unique_ptr<Model::Event::Event>> m_events;
        std::vector<std::unique_ptr<Model::Entity::Enemy>> m_enemies;

    public:
        //constructor
        explicit Game();

        //Game methods
        void newEvent(std::unique_ptr<Model::Event::Event> &event);
        Model::Entity::Character* getCharacterOnBox(Vector2 pos);
        void switchDebug();
        void stop();


        void draw(sf::RenderTarget &target, sf::RenderStates states);

        //getters
        bool isRunning() const;
        int getTick() const;
        Model::Map::Map &getMap();
        Model::Entity::Player& getPlayer();
        std::vector<std::unique_ptr<Model::Entity::Enemy>>& getEnemies();

        //inherited from Updatable
        void update(Gamens::Game &game) override;


    };

}

#endif