#ifndef ANDYFIND_CONTROLLER_H
#define ANDYFIND_CONTROLLER_H

#include <map>
#include <AndyFind/Models/Misc/Direction.h>
#include <SFML/Window/Event.hpp>
#include <AndyFind/Models/Element/Element.h>

using namespace Misc;

namespace Controller {
    class Controller {
    private:
        std::map<Direction, sf::Keyboard::Key> m_dirMap;
        std::map<Model::Element::Element, sf::Keyboard::Key> m_elemMap;

    public:
        Controller();

        void putKey(const Direction d, const sf::Keyboard::Key& k);
        void putKey(Model::Element::Element e, const sf::Keyboard::Key& k);
        bool isDirectionPressed(Misc::Direction dir);
        bool isElementPressed(Model::Element::Element element);
    };
}


#endif //ANDYFIND_CONTROLLER_H
