#ifndef ANDYFIND_DIRECTION_H
#define ANDYFIND_DIRECTION_H

#include <vector>
#include <ostream>

class Vector2;


namespace Misc {

    class Direction{
    public:

        enum DirectionValue{
            Up,
            Right,
            Down,
            Left,
            None
        };

        Direction() = default;
        Direction(DirectionValue v) : m_value(v){}

        Vector2 toVector() const;
        static std::vector<Direction> allDirections();

        operator DirectionValue(){
            return m_value;
        }

        bool operator==(const Direction& a) const { return m_value == a.m_value; }
        bool operator==(const DirectionValue& value) const { return m_value == value; }
        operator DirectionValue() const{
            return m_value;
        }
        bool operator!=(const Direction& a) const { return m_value != a.m_value; }
        bool operator!=(const DirectionValue& value) const { return m_value != value; }
        bool operator<(const Direction a) const { return m_value < a.m_value; }
        friend std::ostream &operator<<(std::ostream &str, const Direction& direction);

        int value() const;

    private:
        DirectionValue m_value;
    };


}




#endif //ANDYFIND_DIRECTION_H
