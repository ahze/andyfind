#ifndef ANDYFIND_UPDATABLE_H
#define ANDYFIND_UPDATABLE_H

#include <SFML/Graphics/Drawable.hpp>

namespace Gamens{
    class Game;
}

namespace Model {

    class Updatable {

    public:
        virtual void update(Gamens::Game &game) = 0;
    };

}

#endif