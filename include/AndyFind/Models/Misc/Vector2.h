#ifndef ANDYFIND_VECTOR2_H
#define ANDYFIND_VECTOR2_H

#include <string>
#include <AndyFind/Models/Misc/Direction.h>
#include <SFML/System.hpp>


class Vector2 {

public:
    int m_x;
    int m_y;
    bool m_isCoordinate;

    Vector2(int x, int y, bool isCoordinate = true);
    Vector2(sf::Vector2f, bool isCoordinate = true);

    template<typename T>
    Vector2(sf::Vector2<T> v, bool isCoordinate = true) :Vector2(v.x, v.y, isCoordinate) {}
    Vector2 toPixel() const;

    Vector2 toCoordinate() const;
    sf::Vector2f vector2F() const;
    bool isOrigin() const;
    double distanceTo(const Vector2& v) const;

    void set(int x, int y);
    void set(const Vector2 &v);

    Vector2& operator+=(const Vector2& right);

    friend std::ostream& operator<<(std::ostream &strm, const Vector2 &v);
};

Vector2 operator-(const Vector2& v);
bool operator!=(const Vector2& v1, const Vector2& v2);
bool operator==(const Vector2& v1, const Vector2& v2);
Vector2 operator+(const Vector2& v1, const Vector2& v2);
Vector2 operator-(const Vector2& v1, const Vector2& v2);


template <typename T>
Vector2 operator+(const Vector2& left, const T& right){
    return {left.m_x + right, left.m_y + right, left.m_isCoordinate};
}

Vector2 operator+(const Vector2& left, const Misc::Direction& right);

template <typename T>
Vector2 operator-(const Vector2& left, const T& right){
    return {left.m_x - right, left.m_y - right, left.m_isCoordinate};
}
template <typename T>
Vector2 operator/(const Vector2& left, const T& right){
    return {left.m_x / right, left.m_y / right, left.m_isCoordinate};
}
Vector2 operator*(const Vector2& left, const Vector2& right);

template <typename T>
Vector2 operator*(const Vector2& left, const T& right){
    return {left.m_x * right, left.m_y * right, left.m_isCoordinate};
}


#endif