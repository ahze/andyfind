#ifndef ANDYFIND_EVENT_H
#define ANDYFIND_EVENT_H

#include <AndyFind/Models/Misc/Updatable.h>

namespace Model{
    namespace Event{
        class Event : public Updatable {
        protected:
            int m_framesNecessary;
            int m_framesPassed;
            int m_firstTick;

        public:
            Event(int frameNecessary);
            Event(Gamens::Game &game, float duration);

            int getFramesNecessary() const;
            void update(Gamens::Game &game) override;
            virtual void whenOver(Gamens::Game &game) = 0;

            //getters
            bool isOver(const Gamens::Game& game) const;
            float getPercentage() const;
        };
    }
}


#endif //ANDYFIND_EVENT_H
