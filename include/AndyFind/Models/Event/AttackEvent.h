#ifndef ANDYFIND_ATTACKEVENT_H
#define ANDYFIND_ATTACKEVENT_H

#include <AndyFind/Models/Event/Event.h>
#include <AndyFind/Models/Entity/Character.h>
#include <AndyFind/Models/Element/Element.h>

namespace Model::Event{
    class AttackEvent : public Event{
    private:
        Model::Entity::Character &m_source, *m_target;
        Misc::Direction m_direction;
        Element::Element m_element;

    public:
        AttackEvent(Gamens::Game &game, Model::Entity::Character &source,
                    Misc::Direction direction, Element::Element element);

        void update(Gamens::Game &game) override;

        void whenOver(Gamens::Game &game) override;
    };
}

#endif //ANDYFIND_ATTACKEVENT_H