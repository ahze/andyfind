#ifndef ANDYFIND_MOVEEVENT_H
#define ANDYFIND_MOVEEVENT_H


#include <AndyFind/Models/Event/Event.h>
#include <AndyFind/Models/Entity/Character.h>

namespace Model{
    namespace Event{
        class MoveEvent : public Event{
        private:
            Model::Entity::Character& m_character;
            Misc::Direction m_direction;
        public:
            MoveEvent(Gamens::Game &game, Model::Entity::Character& c, Misc::Direction dir);

            void whenOver(Gamens::Game &game) override;


            void update(Gamens::Game &game) override;
        };
    }
}


#endif //ANDYFIND_MOVEEVENT_H
