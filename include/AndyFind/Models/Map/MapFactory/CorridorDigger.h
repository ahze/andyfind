#ifndef ANDYFIND_CORRIDORDIGGER_H
#define ANDYFIND_CORRIDORDIGGER_H

#include <SFML/Graphics/RectangleShape.hpp>
#include <AndyFind/Models/Misc/Vector2.h>

namespace Model {
    namespace Map {
        namespace MapFactory {
            /**
             * This class will "dig" between the rooms to make corridors
             * In order to do this, the corridors are numbered as followed
             *  r 0 r 1 r
             *  6   7   8
             *  r 2 r 3 r           r = a room
             *  9   10  11          0, 1, ... a corridor
             *  r 4 r 5 r
             *  We first number the horizontal corridors from left to right and top to bottom
             *  and then do the same for vertical ones
             */
            class CorridorDigger {
            private:
                Vector2 m_size;

                void digOneCorridor(std::pair<sf::RectangleShape, sf::RectangleShape> rooms,
                                    std::vector<sf::RectangleShape> &corridors,
                                    bool horizontal);
                int corridorNumber() const;
            public:
                CorridorDigger(Vector2 size);
                void dig(std::vector<sf::RectangleShape>& rooms);

                std::pair<sf::RectangleShape, sf::RectangleShape>
                getRoomPair(std::vector<sf::RectangleShape> &rooms, int i);

                bool isHorizontal(int i);
            };
        }
    }
}


#endif //ANDYFIND_CORRIDORDIGGER_H
