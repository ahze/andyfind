#ifndef ANDYFIND_SIMPLEMAPFACTORY_H
#define ANDYFIND_SIMPLEMAPFACTORY_H

#include <AndyFind/Models/Map/MapFactory/BoxArrayFactory.h>

namespace Model{
    namespace Map{
        namespace MapFactory{
            class SimpleMapFactory : public BoxArrayFactory{
            public:
                SimpleMapFactory();

                void generateBoxes(std::vector<std::unique_ptr<Model::Map::Boxes::Box>> &boxes) override;
            };
        }
    }
}



#endif //ANDYFIND_SIMPLEMAPFACTORY_H
