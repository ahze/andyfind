#ifndef ANDYFIND_BOXARRAYFACTORY_H
#define ANDYFIND_BOXARRAYFACTORY_H

#include <vector>
#include <AndyFind/Models/Map/Boxes/Box.h>
#include <SFML/Graphics/RectangleShape.hpp>

using namespace Model::Map::Boxes;

namespace Model{
    namespace Map{

        class Map;

        namespace MapFactory{
            class BoxArrayFactory{
            private:
                std::vector<sf::RectangleShape> m_rooms;
                int m_roomCount;

                int getNearestRoom(Vector2 box) const;

                int getMaxWidth() const;
                int getMaxHeight() const;

                sf::RectangleShape getRandomRoom();

            public:
                BoxArrayFactory() = default;

                virtual void generateBoxes(std::vector<std::unique_ptr<Model::Map::Boxes::Box>>& boxes);

            };
        }
    }
}

#endif //ANDYFIND_BOXARRAYFACTORY_H
