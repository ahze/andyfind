#ifndef ANDYFIND_MAP_H
#define ANDYFIND_MAP_H

#include <vector>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include <AndyFind/Models/Entity/Entity.h>
#include <AndyFind/Models/Entity/Character.h>
#include <AndyFind/Models/Map/Boxes/Box.h>
#include <AndyFind/Models/Misc/Direction.h>
#include <AndyFind/Models/Map/MapFactory/BoxArrayFactory.h>

namespace Gamens{
    class Game;
}


namespace Model {
    namespace Map {

        class Map : public Model::Updatable {
        private:
            sf::Color m_wallColor;
            Vector2 m_size;
            std::vector<std::unique_ptr<Model::Map::Boxes::Box>> m_boxes;
            sf::VertexArray m_vertices;
            std::vector<sf::Vertex> m_lines;

            int VectorToInt(const Vector2& v) const;
        public:

            Map();

            bool isInBound(const Vector2& v) const;
            bool canGoThisWay(const Model::Entity::Character& c, Misc::Direction dir);
            std::vector<std::pair<Vector2, Misc::Direction >> boxesAround(const Vector2& v) const;

            std::unique_ptr<Boxes::Box>& getBox(const Vector2& v);
            const sf::Color &getWallColor() const;
            const Vector2 &getSize() const;

            void update(Gamens::Game &game) override;

        public:
            void draw(sf::RenderTarget &target, sf::RenderStates states);

            friend std::ostream& operator<<(std::ostream &strm, const Map &m);
        };

    }
}

#endif
