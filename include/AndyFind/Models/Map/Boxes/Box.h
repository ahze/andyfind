#ifndef ANDYFIND_BOX_H
#define ANDYFIND_BOX_H

#include <AndyFind/Models/Entity/Entity.h>
#include <AndyFind/Models/Element/Element.h>
#include <AndyFind/Models/Entity/Character.h>

namespace Model::Map{
    namespace Boxes{

        class Box : public Model::Entity::Entity{
        protected:
            Model::Element::Element m_element;

        public:
            Box(int x, int y, Model::Element::Element element);

            virtual bool isWall() const;
            virtual bool canStepOn() = 0;
            virtual bool activateEffect(Model::Entity::Character &character) = 0;

            virtual char getChar() = 0;
            Element::Element getElement() const;

        };

    }
}


#endif