#ifndef ANDYFIND_WALL_H
#define ANDYFIND_WALL_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <AndyFind/Models/Entity/Entity.h>

#include <AndyFind/Models/Map/Boxes/Box.h>

namespace Model {
    namespace Map {
        namespace Boxes {

            class Wall : public Box {
            public:

                Wall(int x, int y, Model::Element::Element e);

                bool isWall() const override;

                bool canStepOn() override;
                bool activateEffect(Model::Entity::Character &character) override;

                char getChar() override;

                void update(Gamens::Game &game) override;

            };
        }
    }
}
#endif