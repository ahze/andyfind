#ifndef ANDYFIND_GROUND_H
#define ANDYFIND_GROUND_H


#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Models/Map/Boxes/Box.h>

namespace Gamens{
    class Game;
}

namespace Model {
    namespace Map {
        namespace Boxes {

            class Ground : public Box {
            public:
                Ground(int x, int y, Model::Element::Element e);

                bool canStepOn() override;
                bool activateEffect(Model::Entity::Character &character) override;

                char getChar() override;

                void update(Gamens::Game &game) override;
            };
        }
    }
}

#endif