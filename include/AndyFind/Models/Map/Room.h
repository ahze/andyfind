#ifndef ANDYFIND_ROOM_H
#define ANDYFIND_ROOM_H

#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Models/Element/Element.h>

namespace Model{
    namespace Map{
        class Room {
        private:
            Vector2 m_upLeftCorner, m_size;
            Model::Element::Element m_element;

        public:
            Room(Vector2 &mUpLeftCorner, Vector2 &mSize, Element::Element mElement);

            const Vector2 &getUpLeftCorner() const;

            const Vector2 &getSize() const;

            Element::Element getElement() const;
        };
    }
}



#endif //ANDYFIND_ROOM_H
