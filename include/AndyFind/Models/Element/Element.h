#ifndef ANDYFIND_ELEMENT_H
#define ANDYFIND_ELEMENT_H


#include <SFML/Graphics/Color.hpp>
#include <vector>
#include <ostream>

namespace Model::Element{
    enum Element{
        Red,
        Green,
        Blue
    };

    const static sf::Color colors[] = {sf::Color(255, 140, 130),
                                       sf::Color(130, 255, 140),
                                       sf::Color(140, 130, 255)};


    sf::Color getColor(Element e);
    sf::Color getRawColor(Element e);

    const std::vector<Element> getAllElements();

}


std::ostream &operator<<(std::ostream& str, const Model::Element::Element &element);

#endif //ANDYFIND_ELEMENT_H
