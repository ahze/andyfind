#ifndef ANDYFIND_ENTITY_H
#define ANDYFIND_ENTITY_H


#include <AndyFind/Models/Misc/Vector2.h>
#include <AndyFind/Models/Misc/Updatable.h>
#include <ostream>

namespace Model::Entity{

    class Entity : public Model::Updatable {
    protected:
        Vector2 m_pos, m_drawDiff;

    public:
        Entity(int x, int y);

        const Vector2 getPos() const;
        Vector2 &getDrawDiff();
        void setPos(Vector2& v);
        void setDrawDiff(Vector2& v);

        virtual void draw(sf::RenderTarget &target, sf::RenderStates states){}

        friend std::ostream &operator<<(std::ostream &os, const Entity &entity);
    };
}

#endif

