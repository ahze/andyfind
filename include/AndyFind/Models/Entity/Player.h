#ifndef ANDYFIN_PLAYER_H
#define ANDYFIN_PLAYER_H

#include <AndyFind/Models/Entity/Character.h>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <AndyFind/Controller/Controller.h>


namespace Model::Entity{
    class Player : public Model::Entity::Character {
    private:
        Controller::Controller m_controller;

        Element::Element m_attackElement;
        Direction m_attackDirection;
        bool m_wantAttack;

    public:

        Player(int x, int y);
        void update(Gamens::Game &game) override;

        void draw(sf::RenderTarget &target, sf::RenderStates states) override;
        void drawAttacks();
    };

}


#endif