#ifndef ANDYFIND_ENEMY_H
#define ANDYFIND_ENEMY_H

#include <AndyFind/Models/Entity/Character.h>
#include <AndyFind/Models/Element/Element.h>

namespace Model{
    namespace Entity{
        class Enemy : public Character{
        private:
            Element::Element m_element;

        public:
            Enemy(int x, int y, Element::Element mElement);

            bool playerAround(Gamens::Game &game);

            void update(Gamens::Game &game) override;

            void draw(sf::RenderTarget &target, sf::RenderStates states) override;
        };
    }
}



#endif //ANDYFIND_ENEMY_H
