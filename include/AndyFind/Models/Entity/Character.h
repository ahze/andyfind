#ifndef ANDYFIND_CHARACTER_H
#define ANDYFIND_CHARACTER_H

#include <string>
#include <map>
#include <SFML/Graphics/Shape.hpp>
#include <AndyFind/Models/Entity/Entity.h>
#include <AndyFind/Models/Misc/Direction.h>
#include <AndyFind/Models/Element/Element.h>
#include <ostream>

namespace Model::Entity{

    class Character : public Entity {
    protected:
        int m_hp;
        Misc::Direction m_direction;

        //region vertex array generation
        std::map<std::string, sf::VertexArray> m_vertexArrays;
        int m_outlineThickness;
        sf::VertexArray& getAndCheckVA(const std::string& name, sf::PrimitiveType type, int vertexCount);
        static void makeSquare(sf::VertexArray& va, Vector2 pos, int sideSize, sf::Color color, int& startIndex);
        static void makeSquare(sf::VertexArray& va, Vector2 pos, int sideSize, sf::Color color);
        static void makeRectangle(sf::VertexArray& va, Vector2 pos, Vector2 size, sf::Color color, int& startIndex);
        static void makeRectangle(sf::VertexArray& va, Vector2 pos, Vector2 size, sf::Color color);
        void drawBaseSquare(sf::Color out, sf::Color in);
        void drawHealthTriangles(sf::Color color);
        void drawDirection(sf::Color color);
        //endregion
    public:
        Character(int x, int y, int outlineThickness = 0);

        //functionnong
        void attack(Gamens::Game &game, Misc::Direction direction, Element::Element element);
        void takeDamage(Character &source, Element::Element element);
        void takeDamage(Element::Element element);

        void move(Gamens::Game &game, Misc::Direction dir);


        void draw(sf::RenderTarget &target, sf::RenderStates states) override;

        //getter
        Misc::Direction getDirection() const;

        friend std::ostream &operator<<(std::ostream &os, const Character &character);
    };

}


#endif //ANDYFIND_CHARACTER_H
